import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class FilePreparer {

    // Количество значений цифр в файле фиксировано (c учетом включения 0 будет 21 число)
    public static final int NUM_COUNT = 21;

    // Массив заполненный рандомно
    private Integer[] randomGeneratedRow;

    FilePreparer() {
        /**
         * При создании экземпляра класса будет подготовлен массив,
         * в котором есть значения от 0 до 20 в произвольном порядке
         */

        // Заполнение массива в произвольном порядке
        randomGeneratedRow = new Integer[NUM_COUNT];
        for (int i = 0; i < NUM_COUNT; i++)
            randomGeneratedRow[i] = i;
        List<Integer> intList = Arrays.asList(randomGeneratedRow);
        Collections.shuffle(intList);
        randomGeneratedRow = intList.toArray(randomGeneratedRow);
    }


    public void printOutToFile(String fileName) {
        /**
         * Запись в файл подготовленного массива
         */
        try (FileWriter writer = new FileWriter(fileName, false)) {
            for (int i = 0; i < NUM_COUNT; i++) {
                writer.write(randomGeneratedRow[i].toString());
                writer.append(',');
                writer.append(' ');
            }
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void printNumbers() {
        /**
         * Печать перемешанного массива
         */
        System.out.println("Числа из перемешанного массива:");
        for (Integer n : randomGeneratedRow)
            System.out.print(n + " ");
    }
}
