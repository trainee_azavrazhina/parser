import java.io.FileReader;
import java.io.IOException;
import java.util.*;

class Parser {

    // Строка, считанная с файла
    private String str;

    // Массив чисел из считанной строки
    private Integer[] readNumbers;

    Parser(String fileName) {
        str = "";
        readNumbers = new Integer[FilePreparer.NUM_COUNT];
        try (FileReader reader = new FileReader(fileName);
             Scanner scanner = new Scanner(reader)) {
            while (scanner.hasNextLine())
                str += scanner.nextLine();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public String getLine() {
        return str;
    }

    public void prepareListFromLine() {
        /**
         * Процесс распарсивания
         */
        int i = 0;
        for (String num : str.split(", ")) {
            readNumbers[i] = Integer.parseInt(num);
            i++;
        }
    }

    public void sortAscAndDesc() {
        /**
         * Печать распарсенных из строки чисел по возрастанию, затем по убыванию
         */
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(readNumbers));
        Collections.sort(list);
        System.out.println("Значения по возрастанию:");
        System.out.println(list.toString().replaceAll("^\\[|\\]$", ""));
        Collections.reverse(list);
        System.out.println("Значения по убыванию:");
        System.out.println(list.toString().replaceAll("^\\[|\\]$", ""));
    }

}
