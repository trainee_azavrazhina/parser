class ParserLauncher {
    public static void main(String args[]) {

        // Step 1 Подготовка файла
        FilePreparer filePreparer = new FilePreparer();
        filePreparer.printNumbers();
        filePreparer.printOutToFile("file.txt");

        // Step 2 Прочитать строку файла
        Parser parser = new Parser("file.txt");
        System.out.println("\nСчитанная из файла строка:");
        System.out.println(parser.getLine());

        // Step 3 Парсинг строки из файла
        parser.prepareListFromLine();

        // Step 4 Сортировка 1. По возрастанию 2. По убыванию
        parser.sortAscAndDesc();
    }
}
